import itertools
import json
import flask
import plotly
import psycopg2
import psycopg2.extras
from app.Material import Material
from flask import Blueprint
from plotly.utils import pandas
from sklearn.cluster import KMeans
from app.config import *

app = Blueprint('app', __name__)

COLORS = [
    Material.red,
    Material.indigo,
    Material.lightGreen,
    Material.blue,
    Material.lightBlue,
    Material.teal,
    Material.green,
    Material.lime,
    Material.pink,
    Material.cyan,
    Material.yellow,
    Material.purple,
    Material.amber,
    Material.orange,
    Material.deepOrange,
    Material.brown,
    Material.grey,
    Material.deepPurple,
    Material.blueGrey,
    Material.black,
    Material.white
]

# ------------------------------------------------------------------------------------------------
# --- HELPERS ------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------

def similarity(a, b):
    differences = 0
    for ch1, ch2 in zip(a, b):
        if ch1 != ch2:
            differences += 1
    if (differences == 0):
        return 1
    return (1/differences)

def hex2rgb(hex):
    h = hex.lstrip('#')
    return 'rgb{0:s}'.format(str(tuple(int(h[i:i+2], 16) for i in (0, 2, 4))))

# ------------------------------------------------------------------------------------------------
# --- STATIC -------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------

@app.route('/js/<path:path>')
def send_js(path):
    print(path)
    return flask.send_from_directory('resources/static/js', path)

@app.route('/img/<path:path>')
def send_img(path):
    print(path)
    return flask.send_from_directory('resources/static/img', path)

@app.route("/")
def home():
    context = {'test': 'hello world'}
    return flask.render_template('home.html', **context)


# ------------------------------------------------------------------------------------------------
# --- KMEANS -------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------

@app.route("/cluster/")
@app.route("/cluster/<int:clusters>/")
def cluster(clusters=9):
    db = psycopg2.connect(CONNECTION_STR)
    cur = db.cursor(cursor_factory=psycopg2.extras.DictCursor)

    cur.execute("SELECT MIN(EXTRACT(EPOCH FROM tweets.time)) AS time_start, "
                "       MAX(EXTRACT(EPOCH FROM tweets.time)) AS time_end,"
                "       MAX(tweets.retweet_count) AS max_rt, "
                "       MAX(tweets.favorite_count) AS max_fav "
                "  FROM tweets ")

    result = cur.fetchone()
    time_start = result[0]
    time_end = result[1]
    max_rt = result[2]
    max_fav = result[3]

    # HASHTAGS
    cur.execute("SELECT hashtag_meta.hashtag, "
                "       SUM(tweets.retweet_count) AS retweets, "
                "       SUM(tweets.favorite_count) AS favorites, "
                "       MIN(EXTRACT(EPOCH FROM time)) AS first_occurence, "
                "       MAX(EXTRACT(EPOCH FROM time)) AS last_occurence, "
                "       COUNT(EXTRACT(EPOCH FROM time)) AS occurences,"
                "       VARIANCE(EXTRACT(EPOCH FROM time)) AS date_variance, "
                "       STDDEV_POP(EXTRACT(EPOCH FROM time) / (60*60*24)) AS date_stddev "
                "  FROM hashtag_meta "
                " INNER JOIN tweets ON tweets.tweet_id = hashtag_meta.tweet_id "
                " GROUP BY hashtag_meta.hashtag "
                " ORDER BY hashtag_meta.hashtag ")

    # we build a dataframe with all values normalized to 0..1 ranges
    df = pandas.DataFrame(
        [[
             row[0],                   # hashtag
             row[1] / row[5] / 1000,   # popularity
             row[7],                   # date_stddev

        ] for row in cur.fetchall()], columns=[
            'hashtag',
            'popularity',
            'date_stddev'
        ])
    df.set_index('hashtag', inplace=True, drop=True)

    # cut slices and calculate kmeans
    slices = df
    # df[['retweets', 'favorites', 'occurences', 'first_occurence', 'last_occurence']]
    # slices_norm = (slices - slices.mean()) / (slices.max() - slices.min())
    slices_norm = slices
    kmeans = KMeans(n_clusters=clusters).fit(slices_norm)

    points = []
    i = 0
    point_x = []
    point_y = []
    point_text = []
    point_size = []
    point_color = []
    for hashtag, row in slices_norm.iterrows():
        point_text.append('{0:s}, Cluster {1:d}'.format(hashtag, kmeans.labels_[i]))
        point_x.append(float(row['popularity']+1))
        point_y.append(float(row['date_stddev']+0.01))
        point_size.append(float(8))
        point_color.append('{0:s}'.format(COLORS[kmeans.labels_[i]]))
        i += 1

    graphs = [{
        'data':[
            {
                'x': point_x,
                'y': point_y,
                'hovertext': point_text,
                'size': point_size,
                'mode': 'markers',
                'type': 'scatter',
                'marker': {
                    'color': list(map(hex2rgb, [COLORS[x] for x in kmeans.labels_.tolist()]))
                }
            }
        ],
        'layout': {
            'title':'Average Retweets vs. Occurence Distribution',
            'hovermode': 'closest',
            'xaxis':{
                'title':'Average Retweets (1/1000)',
                'type':'log',
                'autorange':True
            },
            'yaxis': {
                'title':'Std. Deviation: Dates of Occurences',
                'type':'log',
                'autorange':True
            },
            'width': 1100,
            'height': 800
        }
    }]

    ids = ['graph-{}'.format(i) for i, _ in enumerate(graphs)]
    graphJSON = json.dumps(graphs, cls=plotly.utils.PlotlyJSONEncoder)
    context = {'ids': ids, 'graphJSON': graphJSON}

    return flask.render_template('kmeans.html', **context)

# ------------------------------------------------------------------------------------------------
# --- TAGNET -------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------

@app.route("/tagnet/")
def dump(offset=0, count=500):
    db = psycopg2.connect(CONNECTION_STR)
    cur = db.cursor(cursor_factory=psycopg2.extras.DictCursor)

    # HASHTAGS
    cur.execute("SELECT hashtag_meta.hashtag, "
                "       hashtag_meta.tweet_id,"
                "       tweets.retweet_count,"
                "       tweets.handle "
                "  FROM hashtag_meta "
                " INNER JOIN tweets ON tweets.tweet_id = hashtag_meta.tweet_id "
                " ORDER BY hashtag_meta.tweet_id ")
    node_attr_dict = {}
    edge_attr_dict = {}
    nodes = []
    edges = []
    previous_tweet_id = ""
    tags_in_tweet = []
    NODE_ATTR_DEFAULT = { 'size':0, 'color':Material.grey }
    for row in cur.fetchall():
        current_hashtag = row[0]
        tweet_id = row[1]
        handle = row[3]
        if (tweet_id != previous_tweet_id):

            # if we hit a new tweet push last tweets hashtag combinations into a list
            pairs = list(itertools.combinations(tags_in_tweet, 2))

            # build edges between all combinations of hasthags occuring in the current tweet
            # itertools.combinations returns in lexicographic order -> no permutations!
            for pair in pairs:
                edge_source = pair[0]
                edge_target = pair[1]

                # updated edge attributes or add edge if its a new one
                edge_key = '{0:s}{1:s}'.format(edge_source, edge_target)
                if edge_key in edge_attr_dict:
                    edge_attr_dict[edge_key] = {
                        'weight':edge_attr_dict[edge_key]['weight'] + 1,
                        'source':edge_source,
                        'target':edge_target
                    }
                else:
                    edge_attr_dict[edge_key] = {
                        'weight':0,
                        'source':edge_source,
                        'target':edge_target
                    }

                # update nodes to which the current edge is incident
                if edge_source in node_attr_dict:
                    node_attr_dict[edge_source]['size'] += 1
                else:
                    node_attr_dict[edge_source] = NODE_ATTR_DEFAULT
                if edge_target in node_attr_dict:
                    node_attr_dict[edge_target]['size'] += 1
                else:
                    node_attr_dict[edge_target] = NODE_ATTR_DEFAULT

            # reset tag list for new tweet_id
            tags_in_tweet = []

        # if the current hashtag isnt yet in the dict, add it with default attributes
        if not(current_hashtag in node_attr_dict):
            node_attr_dict[current_hashtag] = {
                'size':0
            }
        tags_in_tweet.append(current_hashtag)
        previous_tweet_id = tweet_id

    edge_idx = 0
    for (key, attributes) in edge_attr_dict.items():
        edges.append({
            'id':    'e{0:d}'.format(edge_idx),
            'source': '{0:s}'.format(attributes['source']),
            'target': '{0:s}'.format(attributes['target']),
            'weight': attributes['weight'],
            'color': Material.redLighten3
        })
        edge_idx += 1

    node_idx = 0
    GRID_COLS = 20
    for (tag, attributes) in node_attr_dict.items():
        x_pos = node_idx % GRID_COLS
        y_pos = node_idx / GRID_COLS
        nodes.append({
            'id':    '{0:s}'.format(tag),
            'label': '{0:s} ({1:d})'.format(tag, int(attributes['size'])),
            'x':     float(x_pos),
            'y':     float(y_pos),
            'size':  float(attributes['size']+1),
            'color': Material.redLighten1
        })
        node_idx += 1

    context = {
        'graph':   json.dumps({
            'nodes': nodes,
            'edges': edges,
        })
    }

    return flask.render_template('tagnet.html', **context)

# ------------------------------------------------------------------------------------------------
# --- HISTORY ------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------

@app.route("/chart_g/")
def chart_g():
    db = psycopg2.connect(CONNECTION_STR)
    cursor = db.cursor()
    cursor.execute("SELECT COUNT(hashtag_meta.tweet_id) AS t_tally, "
                "       date_trunc('day', tweets.time) AS t_date "
                "  FROM hashtag_meta "
                " INNER JOIN tweets ON tweets.tweet_id = hashtag_meta.tweet_id "
                " GROUP BY date_trunc('day', tweets.time) "
                " ORDER BY t_date ASC "
    )


    t_tally = []
    t_date = []
    for record in cursor.fetchall():
        t_tally.append(record[0])
        t_date.append(record[1])

    graphs = [
        dict(
            data=[
                dict(
                    y=t_tally,
                    x=t_date,
                    type='bar'
                )
            ],
            layout=dict(
                title='Hashtags per day'
            )
        )
    ]

    # send data to plot-builder template
    ids = ['graph-{}'.format(i) for i, _ in enumerate(graphs)]
    graphJSON = json.dumps(graphs, cls=plotly.utils.PlotlyJSONEncoder)
    context = {'ids': ids, 'graphJSON': graphJSON}
    return flask.render_template('history.html', **context)

# ------------------------------------------------------------------------------------------------
# --- TAG HISTORY --------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------

@app.route("/tagHistory/")
@app.route("/tagHistory/<hashtag>/")
def tag_history(hashtag='Trump2016'):
    tag = '#{}'.format(hashtag)
    print(tag)
    db = psycopg2.connect(CONNECTION_STR)
    cursor = db.cursor()

    cursor.execute("SELECT lower(hashtag_meta.hashtag) AS hashtag_lower, COUNT(hashtag_meta.tweet_id) tally FROM hashtag_meta GROUP BY hashtag_lower ORDER BY tally DESC;")
    result = cursor.fetchall()
    link_list = ['{0:s}'.format(str(entry[0]).lstrip('#')) for entry in result]

    cursor.execute("SELECT COUNT(CASE WHEN hashtag_meta.hashtag ILIKE %s THEN 1 END) AS t_tally, "
                   "       date_trunc('day', tweets.time) AS t_date "
                   "  FROM hashtag_meta "
                   " INNER JOIN tweets ON tweets.tweet_id = hashtag_meta.tweet_id "
                   " GROUP BY date_trunc('day', tweets.time) "
                   " ORDER BY t_date ASC;", (tag,))

    t_tally = []
    t_date = []
    for record in cursor.fetchall():
        t_tally.append(record[0])
        t_date.append(record[1])

    graphs = [
        dict(
            data=[
                dict(
                    y=t_tally,
                    x=t_date,
                    type='bar'
                )
            ],
            layout=dict(
                title='{} usage history'.format(tag)
            )
        )
    ]

    # send data to plot-builder template
    ids = ['graph-{}'.format(i) for i, _ in enumerate(graphs)]
    graphJSON = json.dumps(graphs, cls=plotly.utils.PlotlyJSONEncoder)
    context = {'selected_tag': hashtag, 'tag_link_list': link_list, 'ids': ids, 'graphJSON': graphJSON}
    return flask.render_template('tagHistory.html', **context)
