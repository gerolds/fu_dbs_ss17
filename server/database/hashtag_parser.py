import psycopg2
import re
import sys

DESCRIPTION = "-----------------------------------------------------------------------------\n" \
              "Whats going to happen: All alphanumeric hashtags from election_schema.tweets \n" \
              "will be extracted from the tweets.text and assembled into an insert query for\n" \
              "hashtag_meta.                                                                \n" \
              "-----------------------------------------------------------------------------"

print(DESCRIPTION)

if len(sys.argv) < 1:
    print("USAGE: python3 hashtag_parser.py <sql_target_path>")
    exit()

print("INFO: Connecting database.")
connection_string = "host='localhost' dbname='election' user='' password='' port='5432'"
db = psycopg2.connect(connection_string)
cur = db.cursor()
cur.execute('SELECT * FROM election_schema.tweets')
hashtag_list = []
print("INFO: Extracting hashtags from tweets.")
for row in cur.fetchall():
    text = row[1]
    id = row[6]
    tag_matches = re.findall('\#[a-zA-Z0-9]+', text)
    tag_matches = [(id, tag) for tag in tag_matches]
    if (tag_matches) != []:
        hashtag_list += tag_matches

print("INFO: {0} hashtag usages found.".format(len(hashtag_list)))
hashtag_set = set(hashtag_list)

print("INFO: Truncating existing hashtag_meta.")
cur.execute('DELETE FROM election_schema.hashtag_meta')

print("INFO: {0} entries deleted.".format(cur.rowcount))
print("INFO: Building new hashtag_meta.")
target_path = sys.argv[1]
print("INFO: Writing SQL-output to:  " + target_path)
with open(target_path, 'w', encoding='utf-8') as target:
    for row in hashtag_set:
        sql = "INSERT INTO election_schema.hashtag_meta (tweet_id, hashtag) VALUES (%s, %s);"
        values = [row[0],row[1]]
        cur.execute(sql, values)
        target.write(str(cur.mogrify(sql, values)) + '\n')

    cur.execute("SELECT hashtag FROM election_schema.hashtag_meta")
    tally = cur.rowcount
    cur.execute("SELECT DISTINCT hashtag FROM election_schema.hashtag_meta")
    unique_tally = cur.rowcount

print("INFO: {0} entries added ({1} distinct tags).".format(tally, unique_tally))
print("DONE.")
