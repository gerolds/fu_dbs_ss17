import csv
import sys
import dateutil.parser
import psycopg2
# import progressbar

DESCRIPTION = "-----------------------------------------------------------------------------\n" \
              "What is going to happen: The CSV input is parsed, cleaned and assembled into \n" \
              "SQL insert statements, which are then executed on the election database. Also\n" \
              "the SQL statements are logged into a .sql file.                              \n" \
              "-----------------------------------------------------------------------------"

if len(sys.argv) < 2:
    print("USAGE: python3 tweet_parser.py <csv_source_path> <sql_target_path>")
    exit()

print(DESCRIPTION)

print("INFO: Connecting to database: ")

# connect DB
connection_string = "host='localhost' dbname='election' user='' password='' port='5432'"
db = psycopg2.connect(connection_string)

# get cursor
cursor = db.cursor()
cursor.execute('DELETE FROM election_schema.tweets')

# open source CSV
source_path = sys.argv[1]
print("INFO: Reading CSV-input from: " + source_path)
with open(source_path, newline='', encoding='cp1252') as source:

    # open target file
    target_path = sys.argv[2]
    print("INFO: Writing SQL-output to:  " + target_path)
    with open(target_path, 'w', encoding='utf-8') as target:

        # start reading CSV
        source_reader = csv.DictReader(source, dialect='excel', delimiter=';')
        print("INFO: Parsing CSV...")
        header = source_reader.fieldnames
        print("INFO: Attributes recognized: " + ', '.join(header))

        # build SQL
        id = 0
        for r in source_reader:
            id += 1
            sql = "INSERT INTO election_schema.tweets (" \
                  "tweet_id, " \
                  "handle, " \
                  "text, " \
                  "original_author, " \
                  "time, " \
                  "retweet_count, " \
                  "favorite_count, " \
                  "source_url) " \
                  "VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
            values = (
                id,
                r['handle'],
                r['text'],
                r['original_author'],
                dateutil.parser.parse(r['time'].replace('T', ' ')),
                int(r['retweet_count']),
                int(r['favorite_count']),
                r['source_url']
            )

            # execute SQL and write log to file
            ins = cursor.execute(sql, values)
            target.write(str(cursor.mogrify(sql, values)) + '\n')

        # check results
        cursor.execute("SELECT * FROM election_schema.tweets")
        result = cursor.fetchall()
        print(len(result))
        print('\nINFO: {0} SQL-statements written to {1}'.format(str(id), target_path))
        print('DONE.')
        exit()

