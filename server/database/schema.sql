CREATE TABLE hashtag_meta
(
    tweet_id INTEGER NOT NULL,
    hashtag VARCHAR(140) NOT NULL
);
CREATE TABLE tweets
(
    handle VARCHAR(15) NOT NULL,
    text VARCHAR(140) NOT NULL,
    original_author VARCHAR(15),
    time TIMESTAMP NOT NULL,
    retweet_count INTEGER DEFAULT 0 NOT NULL,
    favorite_count INTEGER DEFAULT 0 NOT NULL,
    tweet_id INTEGER NOT NULL,
    source_url VARCHAR(140)
);
CREATE UNIQUE INDEX hashtags_pkey ON hashtag_meta (tweet_id, hashtag);
CREATE UNIQUE INDEX tweets_pkey ON tweets (tweet_id);
