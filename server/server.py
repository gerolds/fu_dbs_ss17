from flask import Flask

from app.controller import app

DEFAULT_BLUEPRINTS = [app]

app = Flask(__name__, template_folder="resources/views")

for blueprint in DEFAULT_BLUEPRINTS:
    app.register_blueprint(blueprint)

if __name__ == "__main__":
    app.run(None, port=8000, debug=False)
