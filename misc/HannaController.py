import flask
import psycopg2
from flask import Blueprint
from flask import Markup
from flask import Flask
from flask import render_template
import plotly
from pandas import json
from scipy.stats import itemfreq

import json
import flask
import psycopg2
import psycopg2.extras
import plotly
from flask import Blueprint
from app.Controller.Material import Material
from app.Controller.config import *

hanna = Blueprint('hanna',  __name__)

@hanna.route("/chart/")
def allOccurencesChart():
    db = psycopg2.connect(CONNECTION_STR)
    cur = db.cursor(cursor_factory=psycopg2.extras.DictCursor)

    cur.execute("SELECT hashtag_meta.hashtag, tweets.time"
                " FROM hashtag_meta "
                " INNER JOIN tweets ON tweets.tweet_id = hashtag_meta.tweet_id "
                " GROUP BY hashtag_meta.hashtag, time "
                " ORDER BY time ASC "
    )
    result = cur.fetchall()
    timeArray = []
    for entry in range(0,len(result)):
        days = result[entry][1].strftime("%m, %d")
        timeArray = timeArray + [days]

    valueArray = itemfreq(timeArray).tolist()

    for i in range(0, len(valueArray)):
        valueArray[i][1] = int(valueArray[i][1])
    print(valueArray)

    #array aus arrays erstellen, erster Wert ist Tag (zweiter und dritter Wert von Tupel time)
    # zweiter Wert ist Count der Hashtags an einem Tag
    context = {
        'values': valueArray,
        'labels': timeArray
    }
    return render_template('chart.html', **context)

@hanna.route("/chartHashtag/<hashtag>")
def oneOccurenceChart(hashtag):
    db = psycopg2.connect(CONNECTION_STR)
    cur = db.cursor(cursor_factory=psycopg2.extras.DictCursor)
    print(hashtag)
    context = {
        'values': [[1,100],[2, 9],[3, 8],[4, 7],[5, 6],[6, 4],[7, 7],[8, 8]],
        'labels': ["January", "February", "March", "April", "May", "June", "July", "August"]
    }
    return render_template('chart2.html', **context)
