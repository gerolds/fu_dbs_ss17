import psycopg2
import re
import postgresql

# Script to extract all alphanumeric hashtags from election_schema.tweets and
# assembles an insert query for election_schema.hashtag_meta.

db = postgresql.open("pq://localhost/election")
db.execute('DELETE FROM election_schema.word_meta')

conn_string = "host='localhost' dbname='election' user='' password=''"
conn = psycopg2.connect(conn_string)
cursor = conn.cursor()
cursor.execute('SELECT * FROM election_schema.tweets')
wordList = []
i = 0
chsB = [',', ';', ':', '.', '!', '?', '@', '"', 'https//tco/s', 'https//tco/']
chsA = ['-', '—', '_', '/']
for row in cursor.fetchall():
    text = row[1]
    for ch in chsB:
        text = text.replace(ch, '')
    for ch in chsA:
        text = text.replace(ch, ' ')
    tweet_id = row[6]
    #words = re.findall('[a-zA-Z_\']+', text)
    words = re.findall("\S*[a-zA-Z0-9]\S*", text)
    cwords = []
    for w in words:
        cwords.append(w.lower())
    print(cwords)
    i += len(words)
    tweetwords = [(tweet_id, word.lower()) for word in words]
    wordList += tweetwords

wordList = list(set(wordList))

for word in wordList:
    if (True):
        ins = db.prepare("INSERT INTO election_schema.word_meta (tweet_id, word) VALUES ($1, $2)")
        ins(int(word[0]), str(word[1]))


print("INFO: " + str(i) + " words found")
