import re
import psycopg2
import sys


def filter(string, group):
    return p.search(string).group(group)

f = open('electionkopie.csv')
csv = f.read()
f.close()
csv = re.sub(r'\n^(?!(realDonaldTrump)|(HillaryClinton))', r'\\n', csv, flags=re.M)
csv_lines = csv.splitlines()
csv_lines = csv_lines[1:]
p = re.compile(r'(?P<phandle>^([^;]+));'
               r'(?P<ptext>(.+));'
               r'(?P<porig>([^;]*));'
               r'(?P<ptime>([^;]+));'
               r'(?P<pquote>([^;]+));'
               r'(?P<pretC>([^;]+));'
               r'(?P<pfavC>([^;]+));'
               r'(?P<psource>([^;]+))')
handle_set = {filter(i, 'phandle') for i in csv_lines}
orig_handle_set = {filter(i, 'porig') for i in csv_lines if filter(i, 'porig') != ''}
source_set = {filter(i, 'psource') for i in csv_lines if filter(i, 'psource') != ''}

a = 1
sources = {}
for i in source_set:
    sources.update({i: a})
    a += 1

all_handle_set = set(handle_set | orig_handle_set)

text = [(i + 1,
         filter(csv_lines[i], 'ptext'),
         filter(csv_lines[i], 'pretC'),
         filter(csv_lines[i], 'pfavC'))
        for i in range(len(csv_lines))]


tweet = [(i + 1, i + 1,
          sources[filter(csv_lines[i], 'psource')], filter(csv_lines[i], 'ptime'))for i in range(len(csv_lines))]

urlTags = set()

urlTags_list = [re.findall(r'https://t.co/\w+', text[i][1]) for i in range(len(text))]

for i in urlTags_list:
    if i != []:
        urlTags |= set(i)

hashTags = set()

hashTags_list = [re.findall(r'\#\w+', text[i][1]) for i in range(len(text))]

for i in hashTags_list:
    if i != []:
        hashTags |= set(i)

with open('abfragen.sql', 'r', encoding='utf-8') as source:
    linereader = source.readlines()

    try:
        db = psycopg2.connect(
            "dbname='election_nina_friederike' user='postgres' host='localhost' password='password' port='5432'")
        cursor = db.cursor()

        for query in linereader:
            if len(query.strip()) > 0:
                cursor.execute(query.strip())
                print(query.strip())

        db.commit()
        print("Done.")
        # urlTags |= set(i)

    except psycopg2.Error as e:
        print(e)
