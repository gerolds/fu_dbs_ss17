import flask
import psycopg2
import psycopg2.extras
import json
from flask import render_template
from flask import Flask
import plotly
import pandas
import numpy
import scipy
import re
import csv
from sklearn.cluster import KMeans
from sqlalchemy import create_engine

@app.route("/hello/")
def hello():
    context = {'message': 'Hello world!'}
    return render_template('hello.html', **context)

@app.route("/hanna/")
def hanna():
    return "noch in arbeit"

@app.route("/dump/<int:offset>/<int:count>")
def dump(offset=0, count=15):
    connection_string = "host='localhost' dbname='election' user='' password=''"
    db = psycopg2.connect(connection_string)
    cur = db.cursor(cursor_factory=psycopg2.extras.DictCursor)

    # HASHTAGS
    cur.execute("SELECT tweet_id, hashtag FROM election_schema.hashtag_meta LIMIT %s OFFSET %s", [count, offset])
    hashtags = []
    for r in cur.fetchall():
        hashtags.append({
            'tweet_id':r[0],
            'hashtag':r[1],
        })
    hashtags_csv = ','.join(hashtags[0].keys()) + '\n'
    for entry in hashtags:
        esc_str_lst = [('%s'.format(f)).replace('"','\"') for f in entry]
        hashtags_csv += '"%s"\n'.format('","'.join(esc_str_lst))
    hashtags_json = json.dumps(hashtags)

    # TWEETS
    cur.execute("SELECT handle, text, original_author, time, retweet_count, "
                "favorite_count, tweet_id, source_url "
                "FROM election_schema.tweets LIMIT %s OFFSET %s", [count, offset])

    tweets = []
    for r in cur.fetchall():
        tweets.append( {
            'tweet_id':r[6],
            'handle':r[0],
            'text':r[1],
            'original_author':r[2],
            'time':r[3],
            'retweet_count':r[4],
            'favorite_count':r[5],
            'source_url':r[7],
        })
    tweets_csv = ','.join(tweets[0].keys()) + '\n'
    for entry in tweets:
        esc_str_lst = [('{0}'.format(val)).replace('"','\"') for key, val in entry.items()]
        tweets_csv += '"{0}"\n'.format('","'.join(esc_str_lst))

    tweets_json = json.dumps(tweets, cls=plotly.utils.PlotlyJSONEncoder)

    context = {'tweets_json':tweets_json,
               'tweets_csv':tweets_csv,
               'tweets':tweets,
               'hashtags':hashtags,
               'hashtags_json':hashtags_json,
               'hashtags_csv':hashtags_csv,
               'offset':offset,
               'count':count
               }
    #return render_template('debug.html', **context)
    return flask.Response(response=tweets_json, status=200, mimetype='application/json')



@app.route("/entries/")
def entries():
    connection_string = "host='localhost' dbname='election' user='' password=''"
    db = psycopg2.connect(connection_string)
    cur = db.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cur.execute("SELECT tweet_id, handle, text, time FROM election_schema.tweets LIMIT 100")
    entries = list()
    for record in cur:
        print(record)
        entries.append({
            'tweet_id': record[0],
            'handle': record[1],
            'text': record[2],
            'date': record[3]
        })

    context = {'entries': entries}

    return render_template('entries.html', **context)


@app.route("/cluster/")
def cluster():
    connection_string = "host='localhost' dbname='election' user='' password=''"
    db = psycopg2.connect(connection_string)
    cur = db.cursor()
    cur.execute("SELECT "
                "handle, "
                "SUM(retweet_count) AS rt_total, "
                #"extract(EPOCH FROM time) AS t_time, "
                "date_trunc('day', tweets.time) AS t_time "
                "FROM election_schema.tweets "
                "GROUP BY t_time, handle "
                "LIMIT 10000")

    dataframe = pandas.DataFrame(cur.fetchall(), columns=['handle', 'rt_total', 't_time'])
    a = dataframe[dataframe.handle == 'HillaryClinton']
    b = dataframe[dataframe.handle == 'realDonaldTrump']
    kmeans = KMeans(n_clusters=4).fit(dataframe)

    graphs = [
        dict(
            data=[
                dict(
                    x=a['t_time'],
                    y=a['rt_total'],
                    mode='markers',
                    type='scatter'
                ),
                dict(
                    x=b['t_time'],
                    y=b['rt_total'],
                    mode='markers',
                    type='scatter'
                )
            ],
            layout=dict(
                title='Retweets'
            )
        )
    ]
    ids = ['graph-{}'.format(i) for i, _ in enumerate(graphs)]
    graphJSON = json.dumps(graphs, cls=plotly.utils.PlotlyJSONEncoder)

    context = {'ids': ids, 'graphJSON': graphJSON}
    return render_template('plot.html', **context)


@app.route("/volume/")
def volume():
    connection_string = "host='localhost' dbname='election' user='' password=''"
    db = psycopg2.connect(connection_string)
    cur = db.cursor()
    cur.execute("SELECT "
                "COUNT(tweet_id) AS t_tally, "
                "date_trunc('day', tweets.time) AS t_date, "
                "SUM(retweet_count) AS rt_total "
                "FROM election_schema.tweets "
                "GROUP BY date_trunc('day', tweets.time) "
                "LIMIT 10000")
    entries = list()
    for record in cur:
        # print(record)
        entries.append({
            't_tally': record[0],
            't_date': record[1],
            'rt_total': record[2]
        })

    print("entries found: " + str(len(entries)))
    graphs = [
        dict(
            data=[
                dict(
                    x=[r['t_date'] for r in entries],
                    y=[r['t_tally'] for r in entries],
                    type='bar'
                )
            ],
            layout=dict(
                title='Tweets per day'
            )
        )
    ]
    ids = ['graph-{}'.format(i) for i, _ in enumerate(graphs)]
    graphJSON = json.dumps(graphs, cls=plotly.utils.PlotlyJSONEncoder)

    context = {'entries': entries, 'ids': ids, 'graphJSON': graphJSON}

    return render_template('entries.html', **context)