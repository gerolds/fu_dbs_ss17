# [fit] DBS Projekt
__Iteration 2__  
Nicolo Knapp  
Johanna Pfannschmidt  
Gerold Schneider

----

![fill, original](raw.png)

----

![fill, original](erd.pdf)

-----

```sql
CREATE TABLE hashtag_meta
(
    tweet_id    INTEGER NOT NULL,
    hashtag     VARCHAR(140) NOT NULL,
                CONSTRAINT hashtags_pkey
                           PRIMARY KEY (tweet_id, hashtag),
                CONSTRAINT hashtag_tweets__fk
                           FOREIGN KEY (tweet_id)
                           REFERENCES tweets (tweet_id)
);

CREATE TABLE tweets
(
    handle          VARCHAR(15) NOT NULL,
    text            VARCHAR(140) NOT NULL,
    original_author VARCHAR(15),
    time            TIMESTAMP NOT NULL,
    retweet_count   INTEGER DEFAULT 0 NOT NULL,
    favorite_count  INTEGER DEFAULT 0 NOT NULL,
    tweet_id        INTEGER PRIMARY KEY NOT NULL,
    source_url      VARCHAR(140)
);
```

----

# Clean and Insert Data

```python, [.highlight: 7-22]
connection_string = "host='localhost' dbname='election' user='' password=''"
db = psycopg2.connect(connection_string)
cursor = db.cursor()
with open(source_path, newline='', encoding='cp1252') as source:
    source_reader = csv.DictReader(source, dialect='excel', delimiter=';')
    header = source_reader.fieldnames
    for r in source_reader:
        id += 1
        sql = "INSERT INTO election_schema.tweets (" \
              "tweet_id, handle, text, original_author, time, retweet_count, favorite_count, source_url) " \
              "VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
        values = (
            id,
            r['handle'],
            r['text'],
            r['original_author'],
            dateutil.parser.parse(r['time'].replace('T', ' ')),
            int(r['retweet_count']),
            int(r['favorite_count']),
            r['source_url'])
        ins = cursor.execute(sql, values)
```

----

```bash
-----------------------------------------------------------------------------
What is going to happen: The CSV input is parsed, cleaned and assembled into
SQL insert statements, which are then executed on the election database. Also
the SQL statements are logged into a .sql file.                              
-----------------------------------------------------------------------------
INFO: Connecting to database:
INFO: Reading CSV-input from: ../resources/election_tweets.csv
INFO: Writing SQL-output to:  ../sql/tweets.sql
INFO: Parsing CSV...
INFO: Attributes recognized: handle, text, is_retweet, original_author, time, ...
INFO: Processed 6081 lines (Elapsed Time: 0:00:02)                             
INFO: 6126 SQL-statements written to ../sql/tweets.sql
DONE.
```

----

# Extract Hashtags

```python, [.highlight: 6-12, 14-18]
connection_string = "host='localhost' dbname='election' user='' password=''"
db = psycopg2.connect(connection_string)
cur = db.cursor()
cur.execute('SELECT * FROM election_schema.tweets')
hashtag_list = []
for row in cur.fetchall():
    text = row[1]
    id = row[6]
    tag_matches = re.findall('\#[a-zA-Z0-9]+', text)
    tag_matches = [(id, tag) for tag in tag_matches]
    if (tag_matches) != []:
        hashtag_list += tag_matches
hashtag_set = set(hashtag_list)
for row in hashtag_set:
    sql = "INSERT INTO election_schema.hashtag_meta (tweet_id, hashtag) VALUES (%s, %s);"
    values = [row[0],row[1]]
    cur.execute(sql, values)
```

----

```bash
-----------------------------------------------------------------------------
Whats going to happen: All alphanumeric hashtags from election_schema.tweets
will be extracted from the tweets.text and assembled into an insert query for
hashtag_meta.                                                                
-----------------------------------------------------------------------------
INFO: Connecting database.
INFO: Extracting hashtags from tweets.
INFO: 1787 hashtag usages found.
INFO: Truncating existing hashtag_meta.
INFO: 1784 entries deleted.
INFO: Building new hashtag_meta.
INFO: Writing SQL-output to:  ../sql/hashtags.sql
INFO: 1784 entries added (429 distinct tags).
DONE.
```

----



![fit](data.png)

----

```python
import flask
import psycopg2
import psycopg2.extras
import json

app = Flask(__name__)

@app.route("/hashtags/")
@app.route("/hashtags/<int:offset>/<int:count>/")
def dump(offset=0, count=15):
    connection_string = "host='localhost' dbname='election' user='' password=''"
    db = psycopg2.connect(connection_string)
    cur = db.cursor(cursor_factory=psycopg2.extras.DictCursor)

    cur.execute("SELECT tweet_id, hashtag "
                "FROM election_schema.hashtag_meta "
                "ORDER BY hashtag_meta.tweet_id "
                "LIMIT %s OFFSET %s", [count, offset])
    hashtags = []
    for r in cur.fetchall():
        hashtags.append({
            'tweet_id':r[0],
            'hashtag':r[1],
        })
    hashtags_json = json.dumps(hashtags)

    context = {'hashtags_json':hashtags_json,
               'offset':offset,
               'count':count
               }
    return flask.jsonify(hashtags)

if __name__ == "__main__":
    app.run()
```

----

![fit](basic_server.png)

----
#[fit] Thank you.
