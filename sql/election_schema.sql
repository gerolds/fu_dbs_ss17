CREATE TABLE election_schema.tweets
(
    handle VARCHAR(15) NOT NULL,
    text TEXT NOT NULL,
    original_author VARCHAR(15),
    time TIMESTAMP NOT NULL,
    retweet_count INTEGER DEFAULT 0 NOT NULL,
    favorite_count INTEGER DEFAULT 0 NOT NULL,
    tweet_id INTEGER PRIMARY KEY NOT NULL,
    source_url VARCHAR(140)
);
CREATE TABLE election_schema.hashtag_meta
(
    tweet_id INTEGER NOT NULL,
    hashtag VARCHAR(160) NOT NULL,
    CONSTRAINT hashtags_pkey PRIMARY KEY (tweet_id, hashtag),
    CONSTRAINT hashtag_tweets__fk FOREIGN KEY (tweet_id) REFERENCES tweets (tweet_id)
);